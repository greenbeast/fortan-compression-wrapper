! https://stackoverflow.com/questions/18300229/fortran-95-optional-statement-does-not-work-using-ftn95-and-plato
! for optional arguments
module compression
  implicit none
contains
  subroutine gzip(filename, arg)
    character(*) :: filename ! means the length of the filename/arguments are assumed
    character(*), optional :: arg
    character(100) :: input

    if (present(arg)) then
       input = "gzip -"//trim(arg)//" "//trim(filename)
    else
       input = "gzip "//trim(filename)
    endif
    print *, input
    call execute_command_line(input)
  end subroutine gzip

  subroutine gunzip(filename)
    character(*) :: filename
    character(100) :: input

    input = "gunzip "//trim(filename)
    print *, input
    call execute_command_line(input)
  end subroutine gunzip
      
  subroutine zip(zip_name, filename, arg)
    character(*) :: filename, zip_name
    character(*), optional :: arg
    character(100) :: input

    if (present(arg)) then
       input = "zip -"//trim(arg)//" "//trim(zip_name)//" "//trim(filename)
    else
       input = "zip "//trim(zip_name)//" "//trim(filename)
    endif
    print *, input
    call execute_command_line(input)
  end subroutine zip

  subroutine bzip2(filename, arg)
    character(*) :: filename
    character(*), optional :: arg
    character(100) :: input

    if (present(arg)) then
       input = "bzip2 -"//trim(arg)//" "//trim(filename)
    else
       input = "bzip2 "//trim(filename)
    endif
    print *, input
    call execute_command_line(input)
  end subroutine bzip2

  subroutine xz(filename, arg)
    character(*) :: filename
    character(*), optional :: arg
    character(100) :: input

    if (present(arg)) then
       input = "xz -"//trim(arg)//" "//trim(filename)
    else
       input = "xz "//trim(filename)
    endif
    print *, input
    call execute_command_line(input)
  end subroutine xz

  subroutine rar(filename, arg)
    character(*) :: filename
    character(*), optional :: arg
    character(100) :: input

    if (present(arg)) then
       input = "rar "//trim(arg)//" "//trim(filename)
    else
       input = "rar "//trim(filename)
    endif
    print *, input
    call execute_command_line(input)
  end subroutine rar

  subroutine unrar(filename, arg)
    character(*) :: filename
    character(*), optional :: arg
    character(100) :: input

    if (present(arg)) then
       input = "unrar "//trim(arg)//" "//trim(filename)
    else
       input = "unrar "//trim(filename)
    endif
    print *, input
    call execute_command_line(input)
  end subroutine unrar

  ! can't have it as 7zip, compiler doesn't like that.
  subroutine Sevenzip(filename, arg)
    character(*) :: filename
    character(*), optional :: arg
    character(100) :: input

    if (present(arg)) then
       input = "7zip "//trim(arg)//" "//trim(filename)
    else
       input = "7zip "//trim(filename)
    endif
    print *, input
    call execute_command_line(input)
  end subroutine Sevenzip

  subroutine tar(tar_name, filename, arg)
    character(*) :: filename, tar_name
    character(*), optional :: arg
    character(100) :: input

    if (present(arg)) then
       input = "tar "//trim(arg)//" "//trim(tar_name)//" "//trim(filename)
    else
       input = "tar "//trim(tar_name)//" "//trim(filename)
    endif
    print *, input
    call execute_command_line(input)
  end subroutine tar
  
   
end module compression
  
